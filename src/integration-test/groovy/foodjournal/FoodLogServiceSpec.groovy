package foodjournal

import grails.test.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class FoodLogServiceSpec extends Specification {

    FoodLogService foodLogService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new FoodLog(...).save(flush: true, failOnError: true)
        //new FoodLog(...).save(flush: true, failOnError: true)
        //FoodLog foodLog = new FoodLog(...).save(flush: true, failOnError: true)
        //new FoodLog(...).save(flush: true, failOnError: true)
        //new FoodLog(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //foodLog.id
    }

    void "test get"() {
        setupData()

        expect:
        foodLogService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<FoodLog> foodLogList = foodLogService.list(max: 2, offset: 2)

        then:
        foodLogList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        foodLogService.count() == 5
    }

    void "test delete"() {
        Long foodLogId = setupData()

        expect:
        foodLogService.count() == 5

        when:
        foodLogService.delete(foodLogId)
        sessionFactory.currentSession.flush()

        then:
        foodLogService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        FoodLog foodLog = new FoodLog()
        foodLogService.save(foodLog)

        then:
        foodLog.id != null
    }
}
