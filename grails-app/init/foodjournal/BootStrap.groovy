package foodjournal

import myapp.User
import myapp.Role
import myapp.UserRole
class BootStrap {

    def init = { servletContext ->
        def adminRole=Role.findOrSaveWhere(authority:'ROLE_ADMIN')
        def userRole=Role.findOrSaveWhere(authority:'ROLE_USER')
        def admin=User.findOrSaveWhere(username:'anu',password:'anu')
        def user=User.findOrSaveWhere(username:'tanu',password:'tanu')
        def user1=User.findOrSaveWhere(username:'anushree',password:'anushree')
        if(!admin.authorities.contains(adminRole))
        {
            UserRole.create(admin,adminRole,true)
        }
        if(!user.authorities.contains(adminRole))
        {
            UserRole.create(user,userRole,true)
        }
        if(!user1.authorities.contains(adminRole))
        {
            UserRole.create(user1,userRole,true)
        }
    }
    def destroy = {
    }
}
