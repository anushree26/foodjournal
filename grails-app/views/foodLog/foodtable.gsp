<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 2/22/18
  Time: 1:08 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
FRUITS
<table>
    <tr>
        <th>Food Item</th>
        <th>Calories (in g)</th>
        <th>Proteins (in g)</th>
        <th>Carbohydrates (in g) </th>
        <th>Fats (in g)</th>
    </tr>
    <tr>
        <td>Apple</td>
        <td>56</td>
        <td>0.3</td>
        <td>14</td>
        <td>0.2</td>
    </tr>
    <tr>
        <td>Avocado Pear</td>
        <td>190</td>
        <td>2</td>
        <td>9</td>
        <td>15</td>
    </tr>
    <tr>
        <td>Banana</td>
        <td>95</td>
        <td>1.1</td>
        <td>23</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Chickoo</td>
        <td>94</td>
        <td>0.4</td>
        <td>20</td>
        <td>1.1</td>
    </tr>
    <tr>
        <td>Cheries</td>
        <td>70</td>
        <td>1</td>
        <td>12</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Dates</td>
        <td>281</td>
        <td>2.5</td>
        <td>75</td>
        <td>0.4</td>
    </tr>
    <tr>
        <td>Grapes Black</td>
        <td>45</td>
        <td>1</td>
        <td>14</td>
        <td>0</td>
    </tr>
    <tr>
        <td>Guava</td>
        <td>66</td>
        <td>2.6</td>
        <td>14</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Kiwi Fruit</td>
        <td>45</td>
        <td>1.1</td>
        <td>15</td>
        <td>0.5</td>
    </tr>
    <tr>
        <td>Lychies</td>
        <td>61</td>
        <td>0.8</td>
        <td>17</td>
        <td>0.4</td>
    </tr>
    <tr>
        <td>Mangoes</td>
        <td>70</td>
        <td>0.8</td>
        <td>15</td>
        <td>0.4</td>
    </tr>
    <tr>
        <td>Orange</td>
        <td>53</td>
        <td>0.9</td>
        <td>12</td>
        <td>0.1</td>
    </tr>
    <tr>
        <td>Papaya</td>
        <td>32</td>
        <td>0.5</td>
        <td>11</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Peach</td>
        <td>50</td>
        <td>0.9</td>
        <td>10</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Pears</td>
        <td>51</td>
        <td>0.4</td>
        <td>15</td>
        <td>0.1</td>
    </tr>
    <tr>
        <td>Pineapple</td>
        <td>46</td>
        <td>0.5</td>
        <td>13</td>
        <td>0.1</td>
    </tr>
    <tr>
        <td>Plums</td>
        <td>56</td>
        <td>0.7</td>
        <td>7.5</td>
        <td>0.2</td>
    </tr>
    <tr>
        <td>Strawberries</td>
        <td>77</td>
        <td>0.7</td>
        <td>8</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Watermelon</td>
        <td>26</td>
        <td>0.6</td>
        <td>8</td>
        <td>0.2</td>
    </tr>
    <tr>
        <td>Pomegranate</td>
        <td>77</td>
        <td>1.7</td>
        <td>19</td>
        <td>1.2</td>
    </tr>
</table>
VEGETABLES
<table>
    <tr>
        <th>Food Item</th>
        <th>Calories</th>
        <th>Proteins (in g)</th>
        <th>Carbohydrates (in g)</th>
        <th>Fats (in g)</th>
    </tr>
    <tr>
        <td>Brocooli</td>
        <td>25</td>
        <td>2.8</td>
        <td>7</td>
        <td>0.4</td>
    </tr>
    <tr>
        <td>Brinjal</td>
        <td>24</td>
        <td>1</td>
        <td>6</td>
        <td>0.2</td>
    </tr>
    <tr>
        <td>Cabbage</td>
        <td>45</td>
        <td>1.3</td>
        <td>6</td>
        <td>0.1</td>
    </tr>
    <tr>
        <td>Carrot</td>
        <td>48</td>
        <td>0.9</td>
        <td>10</td>
        <td>0.2</td>
    </tr>
    <tr>
        <td>Cauliflower</td>
        <td>30</td>
        <td>1.9</td>
        <td>5</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Mushroom</td>
        <td>18</td>
        <td>3.1</td>
        <td>3.3</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Onion</td>
        <td>50</td>
        <td>1.1</td>
        <td>9</td>
        <td>0.1</td>
    </tr>
    <tr>
        <td>Peas</td>
        <td>93</td>
        <td>5</td>
        <td>14</td>
        <td>0.4</td>
    </tr>
    <tr>
        <td>Potato</td>
        <td>97</td>
        <td>2</td>
        <td>17</td>
        <td>0.1</td>
    </tr>
    <tr>
        <td>Tomato</td>
        <td>21</td>
        <td>0.9</td>
        <td>3.9</td>
        <td>0.2</td>
    </tr>
</table>
CEREALS
<table>
    <tr>
        <th>Food Item</th>
        <th>Calories</th>
        <th>Proteins (in g)</th>
        <th>Carbohydrates (in g)</th>
        <th>Fats (in g)</th>
    </tr>
    <tr>
        <td>Bajra</td>
        <td>360</td>
        <td>12</td>
        <td>68</td>
        <td>5</td>
    </tr>
    <tr>
        <td>Maize flour</td>
        <td>355</td>
        <td>7</td>
        <td>79</td>
        <td>1.8</td>
    </tr>
    <tr>
        <td>Rice</td>
        <td>325</td>
        <td>2.7</td>
        <td>28</td>
        <td>0.3</td>
    </tr>
    <tr>
        <td>Wheat flour</td>
        <td>341</td>
        <td>10</td>
        <td>76</td>
        <td>1</td>
    </tr>
</table>
BREADS per piece
<table>
    <tr>
        <th>Food Item</th>
        <th>Calories</th>
        <th>Proteins (in g)</th>
        <th>Carbohydrates (in g)</th>
        <th>Fats (in g)</th>
    </tr>
    <tr>
        <td>Medium Chapati</td>
        <td>119</td>
        <td>1</td>
        <td>18</td>
        <td>0.1</td>
    </tr>
    <tr>
        <td>1 slice White Bread</td>
        <td>60</td>
        <td>1.8</td>
        <td>10</td>
        <td>0.6</td>
    </tr>
    <tr>
        <td>Paratha (no fillings)</td>
        <td>280</td>
        <td>5.16</td>
        <td>38.94</td>
        <td>8.99</td>
    </tr>
</table>
MILK & MILK PRODUCTS
<table>
    <tr>
        <th>Food Item</th>
        <th>Calories</th>
        <th>Proteins (in g)</th>
        <th>Carbohydrates (in g)</th>
        <th>Fats (in g)</th>
    </tr>
    <tr>
        <td>Butter</td>
        <td>750</td>
        <td>0.9</td>
        <td>0.1</td>
        <td>81</td>
    </tr>
    <tr>
        <td>Buttermilk</td>
        <td>40</td>
        <td>3.3</td>
        <td>4.8</td>
        <td>0.9</td>
    </tr>
    <tr>
        <td>Cheese</td>
        <td>315</td>
        <td>25</td>
        <td>1.3</td>
        <td>33</td>
    </tr>
    <tr>
        <td>Cream</td>
        <td>210</td>
        <td>0.31</td>
        <td>0.42</td>
        <td>5.51</td>
    </tr>
    <tr>
        <td>Ghee</td>
        <td>910</td>
        <td>0.28</td>
        <td>0</td>
        <td>99.48</td>
    </tr>
    <tr>
        <td>Milk Buffalo</td>
        <td>115</td>
        <td>9.15</td>
        <td>12.65</td>
        <td>16.81</td>
    </tr>
    <tr>
        <td>Milk Cow</td>
        <td>42</td>
        <td>3.4</td>
        <td>5</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Milk Skimmed</td>
        <td>90</td>
        <td>8</td>
        <td>13</td>
        <td>0</td>
    </tr>
    <tr>
        <td>Tea</td>
        <td>40</td>
        <td>1</td>
        <td>7</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Coffee</td>
        <td>30</td>
        <td>0.31</td>
        <td>7.14</td>
        <td>0.14</td>
    </tr>

</table>
NON-VEG
<table>
    <tr>
        <th>Food Item</th>
        <th>Calories</th>
        <th>Proteins (in g)</th>
        <th>Carbohydrates (in g)</th>
        <th>Fats (in g)</th>
    </tr>
    <td>Egg</td>
    <td>155</td>
    <td>13</td>
    <td>1.1</td>
    <td>11</td>
</tr>
    <td>Chicken</td>
    <td>239</td>
    <td>27</td>
    <td>0</td>
    <td>14</td>
</tr>
    <td>Meat</td>
    <td>143</td>
    <td>26</td>
    <td>0</td>
    <td>3.5</td>
</tr>
    <td>Fish</td>
    <td>206</td>
    <td>22</td>
    <td>0</td>
    <td>12</td>
</tr>

</table>
</body>
</html>