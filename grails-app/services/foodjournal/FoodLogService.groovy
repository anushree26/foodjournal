package foodjournal

import grails.gorm.services.Service

@Service(FoodLog)
interface FoodLogService {

    FoodLog get(Serializable id)

    List<FoodLog> list(Map args)

    Long count()

    void delete(Serializable id)

    FoodLog save(FoodLog foodLog)

}