package foodjournal

class FoodLog {
    String food
    String types
    Date date
    float proteins
    float calories
    float carbohydrates
    float fats

    static constraints = {
    }
}
