package foodjournal
import grails.plugin.springsecurity.annotation.Secured
@Secured(['ROLE_USER','ROLE_ADMIN'])
class MainController {

    def index() {
        def username="anushree"
        [user:username]
    }
}
