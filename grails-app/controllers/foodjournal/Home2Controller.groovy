package foodjournal

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = false)
@Secured(['ROLE_ADMIN','ROLE_USER'])

class Home2Controller {

    def index() { }
}
