package foodjournal

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = false)
@Secured(['ROLE_ADMIN','ROLE_USER'])

class FoodLogController {

    FoodLogService foodLogService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond foodLogService.list(params), model:[foodLogCount: foodLogService.count()]
    }
    def foodtable() {}
    def chart() {}

    def show(Long id) {
        respond foodLogService.get(id)
    }

    def create() {
        respond new FoodLog(params)
    }

    def save(FoodLog foodLog) {
        if (foodLog == null) {
            notFound()
            return
        }

        try {
            foodLogService.save(foodLog)
        } catch (ValidationException e) {
            respond foodLog.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'foodLog.label', default: 'FoodLog'), foodLog.id])
                redirect foodLog
            }
            '*' { respond foodLog, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond foodLogService.get(id)
    }

    def update(FoodLog foodLog) {
        if (foodLog == null) {
            notFound()
            return
        }

        try {
            foodLogService.save(foodLog)
        } catch (ValidationException e) {
            respond foodLog.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'foodLog.label', default: 'FoodLog'), foodLog.id])
                redirect foodLog
            }
            '*'{ respond foodLog, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        foodLogService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'foodLog.label', default: 'FoodLog'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodLog.label', default: 'FoodLog'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
