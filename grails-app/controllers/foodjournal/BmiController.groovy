package foodjournal

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.annotation.Secured
@Transactional(readOnly = false)
@Secured(['ROLE_ADMIN','ROLE_USER'])
class BmiController {

    def index() { }
    def calculate(){
        float w=Float.parseFloat(params.weight)
        float h=Float.parseFloat(params.height)
        float r=w/(h*h)
        render r
    }
}
